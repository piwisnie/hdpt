package pl.edu.agh.hdpt.model

import pl.edu.agh.hdpt.utils.IpdTest

class StrategiesTest extends IpdTest {

  "Strategies" should {
    "return TitForTat strategy" in {
      val strategy = Strategies.getStrategy("TitForTat")
      strategy.actionProps should be (Map("C" -> 1, "D" -> 0))
      strategy.strategyName should be ("TitForTat")
    }

    "create complete class strategy" in {
      val strategy = Strategies.getStrategy("12_cd_DDCCCDDC")
      strategy.strategyName should be ("12_cd_DDCCCDDC")
      strategy.myMovesLength should be (1)
      strategy.rivalMovesLength should be (2)
      strategy.initMoves should be (List(1, 0))
      strategy.actionProps should be (Map("CCC" -> 0, "CCD" -> 0, "CDC" -> 1, "CDD" -> 1,
        "DCC" -> 1, "DCD" ->0, "DDC" ->0, "DDD" -> 1))

    }
  }

}
