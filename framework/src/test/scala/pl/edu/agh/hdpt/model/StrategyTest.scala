package pl.edu.agh.hdpt.model

import pl.edu.agh.hdpt.actors.PrisonerActor.{Cooperate, Defect}
import pl.edu.agh.hdpt.utils.IpdTest

class StrategyTest extends IpdTest {
  val strategy = new Strategy(initMoves = Array(1), actionProps = Map("C" -> 0, "D" -> 1), myMovesLength = 0, rivalMovesLength = 1)

  "Strategy" should {
    "return next move" in {
      strategy.nextMove() should be(Cooperate)
      strategy.opponentMove(Defect)
      strategy.nextMove() should be(Cooperate)
    }

    "change state after opponent move" in {
      strategy.nextMove() should be(Cooperate)
      strategy.opponentMove(Cooperate)
      strategy.nextMove() should be(Defect)
      strategy.opponentMove(Defect)
      strategy.nextMove() should be(Cooperate)
      strategy.opponentMove(Cooperate)
      strategy.nextMove() should be(Defect)
      strategy.opponentMove(Defect)
      strategy.nextMove() should be(Cooperate)
    }

    "create copy of itself" in {
      val copy = strategy.copy()
      copy.nextMove() should be(Cooperate)
      copy.opponentMove(Cooperate)
      copy.nextMove() should be(Defect)
      copy.opponentMove(Defect)
      copy.nextMove() should be(Cooperate)
      copy.opponentMove(Cooperate)
      copy.nextMove() should be(Defect)
      copy.opponentMove(Defect)
      copy.nextMove() should be(Cooperate)
    }

    "create modified copy of itself" in {
      val copy = strategy.copy(initMoves = Array(0))
      copy.nextMove() should be(Defect)
      copy.opponentMove(Cooperate)
      copy.nextMove() should be(Defect)
      copy.opponentMove(Defect)
      copy.nextMove() should be(Cooperate)
      copy.opponentMove(Cooperate)
      copy.nextMove() should be(Defect)
      copy.opponentMove(Defect)
      copy.nextMove() should be(Cooperate)

      val copy2 = strategy.view().copy(actionProps = Map("C" -> 1, "D" -> 0)
      )
      copy2.nextMove() should be(Cooperate)
      copy2.opponentMove(Cooperate)
      copy2.nextMove() should be(Cooperate)
      copy2.opponentMove(Cooperate)
      copy2.nextMove() should be(Cooperate)
      copy2.opponentMove(Defect)
      copy2.nextMove() should be(Defect)
      copy2.opponentMove(Defect)
      copy2.nextMove() should be(Defect)
      copy2.opponentMove(Cooperate)
      copy2.nextMove() should be(Cooperate)
    }

    "count cooperativity" in {
      Strategies.allDefect.cooperativity should be(0.0)
      Strategies.titForTat.cooperativity should be(1.0)
      Strategies.allCooperate.cooperativity should be(1.0)
      strategy.cooperativity should be(0.5)
      for (_ <- 0 until 100) {
        val randomStrategy = Strategies.random
        val randomCooperativity = randomStrategy.cooperativity
        val condition = randomCooperativity >= 0 && randomCooperativity <= 1
        if (!condition) println(randomStrategy)
        condition should be(true)
      }
    }
  }
}
