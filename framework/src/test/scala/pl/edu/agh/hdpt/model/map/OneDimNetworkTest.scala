package pl.edu.agh.hdpt.model.map

import pl.edu.agh.hdpt.utils.IpdTest

class OneDimNetworkTest extends IpdTest {
  val networkMap = new OneDimNetwork
  networkMap.init(10, Map.empty)

  "OneDimNetworkTest" should {
    "findNeighbours" in {
      networkMap.findNeighbours(0) should be(Seq(1))
      networkMap.findNeighbours(5) should contain theSameElementsAs Seq(4, 6)
      networkMap.findNeighbours(9) should be(Seq(8))
    }
  }
}
