package pl.edu.agh.hdpt.model

import pl.edu.agh.hdpt.actors.PrisonerActor.{Cooperate, Defect}
import pl.edu.agh.hdpt.utils.IpdTest

import scala.concurrent.duration.DurationInt
import scala.language.postfixOps
import scala.util.Random

class MutationsTest extends IpdTest {
  implicit val patience: PatienceConfig = PatienceConfig(3 seconds, 0 milliseconds)
  val strategy = Strategies.ccCCCDDDDC


  "Mutations" should {
    "change random action probability in Strategy" in {
      eventually({
        val copy = strategy.copy()
        val mutated = Mutations.changeProbability(copy)

        eventually {
          val opponent = if (Random.nextBoolean()) Cooperate else Defect
          copy.opponentMove(opponent)
          mutated.opponentMove(opponent)

          mutated.nextMove() shouldNot be(copy.nextMove())
        }
      })
    }

    "change random init move in Strategy" in {
      val copy = strategy.copy()
      val mutated = Mutations.changeInitMove(copy)
      var otherMoves = false

      for (_ <- 0 to copy.initMoves.length + 1) {
        val opponent = if (Random.nextBoolean()) Cooperate else Defect
        copy.opponentMove(opponent)
        mutated.opponentMove(opponent)

        if (mutated.nextMove() != copy.nextMove()) {
          otherMoves = true
        }
      }
      otherMoves shouldBe true
    }

  }
}
