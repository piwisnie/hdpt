package pl.edu.agh.hdpt.sequential

import pl.edu.agh.hdpt.actors.PrisonerActor.{Cooperate, Defect}
import pl.edu.agh.hdpt.model.Strategies
import pl.edu.agh.hdpt.utils.IpdTest

class PrisonerTest extends IpdTest {
  val rival = Strategies.allDefect
  "PrisonerTest" should {
    "play according to strategy" in {
      val prisoner = new Prisoner(0, 10, Strategies.allCooperate)

      prisoner.nextAction() should be(Cooperate)
      prisoner.payoff(3, Cooperate) should be(false)
      prisoner.nextAction() should be(Cooperate)
      prisoner.payoff(-1, Defect) should be(false)
      prisoner.nextAction() should be(Cooperate)
    }

    "updateStrategy" in {
      val prisoner = new Prisoner(0, 10, Strategies.allCooperate)
      prisoner.nextAction() should be(Cooperate)

      prisoner.updateStrategy(Strategies.allCooperate, None)
      prisoner.nextAction() should be(Cooperate)
      prisoner.payoff(3, Cooperate) should be(false)
      prisoner.nextAction() should be(Cooperate)
      prisoner.payoff(-1, Defect) should be(false)
      prisoner.nextAction() should be(Cooperate)

      prisoner.updateStrategy(Strategies.allDefect, None)
      prisoner.nextAction() should be(Defect)
    }

    "return true for payoff if his energy equals 0" in {
      val prisoner = new Prisoner(0, 10, Strategies.allCooperate)
      prisoner.payoff(-10, Cooperate) should be(true)
    }

    "return true for payoff if his energy is below 0" in {
      val prisoner = new Prisoner(0, 10, Strategies.allDefect)
      prisoner.payoff(-15, Cooperate) should be(true)
    }

    "reset strategy and energy" in {
      val prisoner = new Prisoner(0, 10, Strategies.allDefect)
      prisoner.payoff(-15, Cooperate) should be(true)
      prisoner.reset(Strategies.allCooperate, 20)
      prisoner.payoff(-15, Cooperate) should be(false)
      prisoner.payoff(-15, Cooperate) should be(true)
      prisoner.nextAction() should be(Cooperate)
    }
  }
}
