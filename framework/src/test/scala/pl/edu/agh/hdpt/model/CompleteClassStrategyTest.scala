import pl.edu.agh.hdpt.actors.PrisonerActor.{Cooperate, Defect}
import pl.edu.agh.hdpt.model.{Strategies, Strategy}
import pl.edu.agh.hdpt.utils.IpdTest

class CompleteClassStrategyTest extends IpdTest {
  val allCooperate: Strategy = Strategies.allCooperate
  val allDefect: Strategy = Strategies.allDefect
  val titForTat: Strategy = Strategies.titForTat
  val m12_ccCCCDDDDC: Strategy = Strategies.ccCCCDDDDC

  "AllCooperate" should {
    "cooperate with others" in {
      allCooperate.nextMove() should be(Cooperate)
      allCooperate.opponentMove(Cooperate)

      allCooperate.nextMove() should be(Cooperate)
      allCooperate.opponentMove(Defect)

      allCooperate.nextMove() should be(Cooperate)
    }
  }

  "AllDefect" should {
    "defect with others" in {
      allDefect.nextMove() should be(Defect)
      allDefect.opponentMove(Cooperate)

      allDefect.nextMove() should be(Defect)
      allDefect.opponentMove(Defect)

      allDefect.nextMove() should be(Defect)
    }
  }

  "TitForTat" should {
    "cooperate than repeat rival moves" in {
      titForTat.nextMove() should be(Cooperate)
      titForTat.opponentMove(Cooperate)

      titForTat.nextMove() should be(Cooperate)
      titForTat.opponentMove(Defect)

      titForTat.nextMove() should be(Defect)
    }
  }
  

  "mem12_ccCCCDDDDC" should {
    "play according to strategy" in {
      //cc
      m12_ccCCCDDDDC.nextMove() should be (Cooperate)
      m12_ccCCCDDDDC.opponentMove(Cooperate)
      m12_ccCCCDDDDC.nextMove() should be (Cooperate)


      //C | CC -> C
      m12_ccCCCDDDDC.opponentMove(Cooperate)
      m12_ccCCCDDDDC.nextMove() should be (Cooperate)

      //C | CD -> C
      m12_ccCCCDDDDC.opponentMove(Defect)
      m12_ccCCCDDDDC.nextMove() should be (Cooperate)

      //C | DC -> C
      m12_ccCCCDDDDC.opponentMove(Cooperate)
      m12_ccCCCDDDDC.nextMove() should be (Cooperate)

      //C | DD -> D
      m12_ccCCCDDDDC.opponentMove(Defect)
      m12_ccCCCDDDDC.nextMove() should be (Cooperate)
      m12_ccCCCDDDDC.opponentMove(Defect)
      m12_ccCCCDDDDC.nextMove() should be (Defect)

      //D | CC -> D
      m12_ccCCCDDDDC.opponentMove(Cooperate)
      m12_ccCCCDDDDC.nextMove() should be (Defect)
      m12_ccCCCDDDDC.opponentMove(Cooperate)
      m12_ccCCCDDDDC.nextMove() should be (Defect)

      //D | CD -> D
      m12_ccCCCDDDDC.opponentMove(Defect)
      m12_ccCCCDDDDC.nextMove() should be (Defect)

      //D | DC -> D
      m12_ccCCCDDDDC.opponentMove(Cooperate)
      m12_ccCCCDDDDC.nextMove() should be (Defect)

      //D | DD -> C
      m12_ccCCCDDDDC.opponentMove(Defect)
      m12_ccCCCDDDDC.nextMove() should be (Defect)
      m12_ccCCCDDDDC.opponentMove(Defect)
      m12_ccCCCDDDDC.nextMove() should be (Cooperate)

    }

    "introduce itself nicely" in {
      println(m12_ccCCCDDDDC.toString)
    }
  }
}
