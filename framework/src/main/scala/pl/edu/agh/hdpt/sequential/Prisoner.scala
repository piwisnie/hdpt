package pl.edu.agh.hdpt.sequential

import pl.edu.agh.hdpt.actors.PrisonerActor
import pl.edu.agh.hdpt.actors.PrisonerActor.{Action, Energy}
import pl.edu.agh.hdpt.model.{Strategy, StrategyView}

import scala.collection.mutable

class Prisoner(val index: Int, private var _energy: PrisonerActor.Energy, var strategy: Strategy) {
  private val initialStrategy = strategy.strategyName
  private val lastMovesCount = 100
  private var moveInd: Int = 0
  private val _lastMoves = mutable.ArrayBuffer.fill[PrisonerActor.Action](lastMovesCount)(null)

  def lastMoves: Seq[PrisonerActor.Action] =
    _lastMoves

  def updateStrategy(s: Strategy, energy: Option[PrisonerActor.Energy]): Unit = {
    strategy = s
    energy.foreach(_energy = _)
  }

  def strategyView(): StrategyView =
    strategy.view()

  def energy: PrisonerActor.Energy =
    _energy

  /**
    * @return true if prisoner's energy is below or equal 0
    */
  def payoff(p: PrisonerActor.Energy, opponentMove: PrisonerActor.Action): Boolean = {
    strategy.opponentMove(opponentMove)
    _energy += p
    _energy <= 0
  }

  def nextAction(): PrisonerActor.Action = {
    val move = strategy.nextMove()
    _lastMoves(moveInd) = move
    moveInd = (moveInd + 1) % lastMovesCount
    move
  }

  def reset(s: Strategy, initialEnergy: Energy): Unit = {
    _energy = initialEnergy
    strategy = s
  }

  def getStrategyName(): String = {
    strategy.toString
  }

}
