package pl.edu.agh.hdpt.model

import pl.edu.agh.hdpt.actors.PrisonerActor

import scala.math.max
import scala.util.Random

trait StrategyView extends Serializable {

  import Strategy._

  var initMoves: Array[Double]
  val actionProps: Map[String, Double]
  val myMovesLength: Int
  val rivalMovesLength: Int
  val strategyName: String
  val payoffProbs: Array[Double]

  /* Returns new strategy based on the strategy view */
  def copy(initMoves: Array[Double] = this.initMoves, actionProps: Map[String, Double] = this.actionProps,
           myMovesLength: Int = this.myMovesLength, rivalMovesLength: Int = this.rivalMovesLength,
           strategyName: String = this.strategyName, ipdProbs: Array[Double] = this.payoffProbs): Strategy

  override def toString: String = {
    "\tpayoffProbs: [" + payoffProbs + "]\n" +
      "\tinit moves: [" + initMoves.mkString(", ") + "]\n" +
      "\taction probabilities: \n\t" + actionProps.mkString("\n\t")
  }

  lazy val cooperativity: Cooperativity = {
    val fights = 20
    val strategy = copy()

    val cooperations: Double = (0 until fights).count { _ =>
      val move = strategy.nextMove()
      strategy.opponentMove(move)
      move == PrisonerActor.Cooperate
    }
    cooperations / fights
  }
}

object Strategy {
  type Cooperativity = Double

  class State(val move: PrisonerActor.Action)

  case class Transition(afterCooperation: State, afterDefection: State) {

    import pl.edu.agh.hdpt.actors.PrisonerActor._

    def nextState(action: Action): State = action match {
      case Cooperate => afterCooperation
      case Defect => afterDefection
      case default => afterCooperation
    }
  }

  //TODO implement
  def crossover(previousStrategy: StrategyView, betterStrategy: StrategyView, leavePreviousStrategy: Boolean = true): Strategy = {

    val crossoverProps = if (Random.nextBoolean()) previousStrategy.actionProps else betterStrategy.actionProps
    val crossoverInitMoves = if (Random.nextBoolean()) previousStrategy.initMoves else betterStrategy.initMoves
    val crossoverPayoffProbs = if (Random.nextBoolean()) previousStrategy.payoffProbs else betterStrategy.payoffProbs
    val crossoverStrategyName = if (leavePreviousStrategy) previousStrategy.strategyName else betterStrategy.strategyName
    new Strategy(actionProps = crossoverProps, initMoves = crossoverInitMoves, payoffProbs = crossoverPayoffProbs, strategyName = crossoverStrategyName)
  }
}

class Strategy(override val actionProps: Map[String, Double] = Map("C" -> 1, "D" -> 1),
               override var initMoves: Array[Double] = Array(1.0), val myMovesLength: Int = 0, val rivalMovesLength: Int = 1,
               minimize: Boolean = true, val strategyName: String = "default", val payoffProbs: Array[Double] = Array(0.33, 0.33, 0.34)) extends StrategyView {

  import pl.edu.agh.hdpt.actors.PrisonerActor._

  private var initMovesIndex = 0
  private var myMoves: List[Action] = List()
  private var rivalMoves: List[Action] = List()

  /* Returns the next move from strategy. */
  def nextMove(): Action = {
    var nextMove: Action = Cooperate
    val moveIndex: Int = max(myMoves.length, rivalMoves.length)
    if (initMoves.length > initMovesIndex) {
      nextMove = if (initMoves(initMovesIndex) > Random.nextDouble()) Cooperate else Defect
      initMovesIndex += 1
      myMoves = nextMove :: myMoves
      if (myMoves.length > myMovesLength) {
        myMoves = myMoves.dropRight(1)
      }
    } else {
      val actionKey = createActionKey(myMoves, rivalMoves)
      nextMove = if (actionProps(actionKey) > Random.nextDouble()) Cooperate else Defect
      myMoves = nextMove :: myMoves
      if (myMoves.length > myMovesLength) {
        myMoves = myMoves.dropRight(1)
      }
    }
    nextMove
  }

  def createActionKey(myMoves: List[Action], rivalMoves: List[Action]): String = {
    var actionKey = ""
    for (move <- myMoves) {
      actionKey = actionKey.concat(if (move == Cooperate) "C" else "D")
    }
    for (move <- rivalMoves) {
      actionKey = actionKey.concat(if (move == Cooperate) "C" else "D")
    }
    actionKey
  }


  /* Modifies strategy basing on the opponent move. */
  def opponentMove(action: Action): Unit = {
    rivalMoves = action :: rivalMoves
    if (rivalMoves.length > rivalMovesLength) {
      rivalMoves = rivalMoves.dropRight(1)
    }
  }

  /* Returns an immutable view of the strategy state. */
  def view(): StrategyView =
    this

  override def copy(initMoves: Array[Double] = this.initMoves, actionProps: Map[String, Double] = this.actionProps,
                    myMovesLength: Int = this.myMovesLength, rivalMovesLength: Int = this.rivalMovesLength,
                    strategyName: String = this.strategyName, payoffProbs: Array[Double] = this.payoffProbs): Strategy =
    new Strategy(actionProps, initMoves, myMovesLength, rivalMovesLength, strategyName = strategyName, payoffProbs = payoffProbs)
}