package pl.edu.agh.hdpt.sequential

import pl.edu.agh.hdpt.actors.PrisonerActor
import pl.edu.agh.hdpt.config.IpdConfig.Payoffs
import pl.edu.agh.hdpt.model.Strategies

import scala.language.postfixOps

object OneVsOne {
  def main(args: Array[String]): Unit = {
    val payoffs = Payoffs(2, 1, -1, -2)

    for (neighbourStrategy <- Seq(
      Strategies.allCooperate, Strategies.allDefect, Strategies.titForTat
    )) {
      val prisonerA = new Prisoner(0, 0, Strategies.titForTat)
      val prisonerB = new Prisoner(0, 0, neighbourStrategy)

      println(prisonerA.getStrategyName(), prisonerB.getStrategyName())
      for (_ <- 1 to 20) {
        val moveA = prisonerA.nextAction()
        val moveB = prisonerB.nextAction()

        prisonerA.strategy.opponentMove(moveB)
        prisonerB.strategy.opponentMove(moveA)

        val (payoffA, payoffB) = PrisonerActor.Action.resolveFight(payoffs)(moveA, moveB)
        prisonerA.payoff(payoffA, moveB)
        prisonerB.payoff(payoffB, moveA)

        println(moveA, moveB, prisonerA.energy, prisonerB.energy)
      }

      println(prisonerA.energy, prisonerB.energy)
      println()
      println()
    }
  }
}
