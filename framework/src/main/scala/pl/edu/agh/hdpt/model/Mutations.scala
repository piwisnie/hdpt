package pl.edu.agh.hdpt.model

import com.typesafe.scalalogging.LazyLogging

import scala.util.Random

object Mutations extends LazyLogging {
  def mutate(strategyView: StrategyView): Strategy =
    Random.nextInt(3) match {
        case 0 => changeProbability(strategyView)
        case 1 => changeInitMove(strategyView)
        case 2 => changePayoffs(strategyView)
      }

  def changeProbability(copy: StrategyView): Strategy = {
    val randomKey = copy.actionProps.keys.toList(Random.nextInt(copy.actionProps.size))
    val randomValue = calculateRandomValue(copy.actionProps(randomKey))

    val newActionProps = copy.actionProps.updated(randomKey, randomValue)

    return new Strategy(actionProps = newActionProps, initMoves = copy.initMoves,
      myMovesLength = copy.myMovesLength, rivalMovesLength = copy.rivalMovesLength, strategyName = copy.strategyName, payoffProbs = copy.payoffProbs)
  }


  def changeInitMove(copy: StrategyView): Strategy = {
    val randomIndex = Random.nextInt(copy.initMoves.length)
    val newInitMoves = copy.initMoves.clone()
    newInitMoves(randomIndex) = calculateRandomValue(copy.initMoves(randomIndex))
    return new Strategy(actionProps = copy.actionProps, initMoves = newInitMoves,
      myMovesLength = copy.myMovesLength, rivalMovesLength = copy.rivalMovesLength, strategyName = copy.strategyName, payoffProbs = copy.payoffProbs)
  }

  def changePayoffs(copy: StrategyView): Strategy = {

    val ipdProbs = Random.nextDouble()
    val hdptProbs = (1 - ipdProbs) * Random.nextDouble()
    val otherProbs = 1 - ipdProbs - hdptProbs
    val payoffProbs = Array(ipdProbs, hdptProbs, otherProbs)
    return new Strategy(actionProps = copy.actionProps, initMoves = copy.initMoves,
      myMovesLength = copy.myMovesLength, rivalMovesLength = copy.rivalMovesLength, strategyName = copy.strategyName, payoffProbs = payoffProbs)
  }

  def calculateRandomValue(currentValue: Double): Double = {
    var randomValue = currentValue - 0.2 + Random.nextDouble() * 0.4
    if (randomValue < 0.0) {
      randomValue = 0.0
    } else if (randomValue > 1.0) {
      randomValue = 1.0
    }
    randomValue
  }

}
