package pl.edu.agh.hdpt.actors
package map

import akka.actor.ActorSelection
import pl.edu.agh.hdpt.model.map.OneDimNetwork

class OneDimArenaMapActor(p: Seq[ActorSelection], val mapOptions: Map[String, Any]) extends ArenaMapActor(p) {
  override protected val networkMap = new OneDimNetwork
  networkMap.init(p.length, mapOptions)
}
