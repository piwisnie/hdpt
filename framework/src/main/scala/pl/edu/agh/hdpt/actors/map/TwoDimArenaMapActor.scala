package pl.edu.agh.hdpt.actors
package map

import akka.actor.ActorSelection
import pl.edu.agh.hdpt.model.map.TwoDimNetwork

class TwoDimArenaMapActor(p: Seq[ActorSelection], val mapOptions: Map[String, Any]) extends ArenaMapActor(p) {
  override protected val networkMap = new TwoDimNetwork
  networkMap.init(p.length, mapOptions)
}
