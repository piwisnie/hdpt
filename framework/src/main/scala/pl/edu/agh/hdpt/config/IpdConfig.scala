package pl.edu.agh.hdpt.config

import pl.edu.agh.hdpt.actors.PrisonerActor
import pl.edu.agh.hdpt.actors.splitter.ArenaMapSplitter
import pl.edu.agh.hdpt.config.IpdConfig.Payoffs
import pl.edu.agh.hdpt.model.{Strategies, Strategy}

case class IpdConfig(populationSize: Int,
                     private val _initialStrategy: String,
                     initialStrategies: Map[String, Any],
                     separateStrategies: Boolean,
                     initialEnergy: PrisonerActor.Energy,
                     ipdPayoffs: Payoffs,
                     hawkDovePayoffs: Payoffs,
                     otherPayoffs: Payoffs,
                     mapSplitterName: String,
                     splitterOptions: Map[String, Any],
                     useStubs: Boolean,
                     mapName: String,
                     mapOptions: Map[String, Any],
                     crossoverMapName: String,
                     crossoverMapOptions: Map[String, Any],
                     targetFightsToMutationsRatio: Double,
                     fightMgrsCount: Int,
                     crossoverMgrsCount: Int,
                     mutationMgrsCount: Int,
                     replaceEnergyOnMutation: Boolean,
                     forceEpochDeaths: Boolean,
                     fightTimeoutMs: Int,
                     continuationProbability: Double,
                     stubSyncIntervalMs: Int,
                     stubSyncBatchThreshold: Int,
                     localStatsIntervalMs: Int,
                     masterStatsIntervalMs: Int,
                     topNStrategies: Int,
                     leavePreviousStrategy: Boolean,
                     availableHosts: Seq[String]) {

  def mapSplitter: Class[ArenaMapSplitter] = Class.forName(mapSplitterName).asInstanceOf[Class[ArenaMapSplitter]]
  def map: Class[_] = Class.forName(mapName)
  def crossoverMap: Class[_] = Class.forName(crossoverMapName)
  def initialStrategy(): Strategy = Strategies.getStrategy(_initialStrategy).copy()
  def nextStrategy(strategyName: String): Strategy = Strategies.getStrategy(strategyName).copy()

  def validate(): Unit = {
    require(populationSize > fightMgrsCount)
    require(populationSize > mutationMgrsCount)
    require(fightTimeoutMs > 0)
  }
}

object IpdConfig {
  import PrisonerActor._

  case class Payoffs(temptation: Energy, reward: Energy, punishment: Energy, sucker: Energy)
}
