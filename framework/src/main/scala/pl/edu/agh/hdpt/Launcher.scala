package pl.edu.agh.hdpt

import java.io.{File, FileWriter}
import java.util.UUID
import java.util.concurrent.TimeUnit

import akka.actor.{ActorSelection, ActorSystem}
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging
import pl.edu.agh.hdpt.actors.MasterStatsActor.MasterStats
import pl.edu.agh.hdpt.actors.{MasterArenaActor, MasterStatsActor}
import pl.edu.agh.hdpt.config.{IpdConfig, IpdFileConfigLoader}
import pl.edu.agh.hdpt.sequential.StrategyStatistics

import scala.collection.immutable.Seq
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{DurationInt, FiniteDuration}
import scala.language.postfixOps
import scala.util.{Success, Try}

object Launcher extends StrictLogging {
  def writeInitLine(filename: String, energyPerStrategy: Map[String, Any] = Map.empty): Unit = {
    var line = Seq(
      "uuid",
      "fromStart",
      "allFights",
      "ipdFights",
      "hawkDoveFights",
      "otherFights",
      "coopRate"
    ).map(_.replaceAll("\\t", "\\t").replaceAll("\\n", "\\n").replaceAll("\"", "\\\"")).map(s => s""""$s"""").mkString(",")

    for ((strategyName, strategyEnergy) <- energyPerStrategy) {
      line = line.concat(",\"").concat("Prisoners: " + strategyName).concat("\"")
    }
    for ((strategyName, strategyEnergy) <- energyPerStrategy) {
      line = line.concat(",\"").concat("Coop: " + strategyName).concat("\"")
    }
    for ((strategyName, strategyEnergy) <- energyPerStrategy) {
      line = line.concat(",\"").concat("Energy: " + strategyName).concat("\"")
    }

    new FileWriter(filename, false) {
      write(line)
      write("\n")
      close()
    }
  }

  def writeCsvStats(filename: String, uuid: UUID, sequential: Boolean, config: IpdConfig, stats: MasterStats, fromStart: Long,
                    `final`: Boolean, warmUp: FiniteDuration, end: FiniteDuration, finalStats: FiniteDuration,
                    energyPerStrategy: Map[String, StrategyStatistics] = Map.empty): Unit = {

    var line = Seq(
      uuid,
      //sequential,
      fromStart,
      //`final`,
      //      warmUp.toSeconds, end.toSeconds, finalStats.toSeconds,
      //      stats.prisonersCount,
      //      stats.energySum, stats.energyMax, stats.energyAvg, stats.energyMin,
      stats.allFights,
      stats.ipdFights,
      stats.hawkDoveFights,
      stats.otherFights,
      //stats.fightsSum, stats.fightsMax, stats.fightsAvg, stats.fightsMin,
      //      stats.allMutations, stats.mutationsSum, stats.mutationsMax, stats.mutationsAvg, stats.mutationsMin,
      //      stats.allTimeouts, stats.remoteTimeouts, stats.remoteFightsStarted,
      //      stats.lastCooperationsCount, stats.lastMovesCount,
      BigDecimal((stats.lastCooperationsCount * 1.0) / stats.lastMovesCount).setScale(4, BigDecimal.RoundingMode.HALF_UP)
      //stats.deaths,
      //      BigDecimal(stats.cooperativityAvg).setScale(4,BigDecimal.RoundingMode.HALF_UP),
      //      config.populationSize, config.initialEnergy, config.separateStrategies,
      //      config.payoffs.temptation, config.payoffs.reward, config.payoffs.punishment, config.payoffs.sucker,
      //      config.mapSplitterName, config.splitterOptions, config.useStubs,
      //      config.mapName, config.mapOptions,
      //      config.crossoverMapName, config.crossoverMapOptions,
      //      config.targetFightsToMutationsRatio, config.crossoverMgrsCount,
      //      config.fightMgrsCount, config.mutationMgrsCount, config.replaceEnergyOnMutation, config.forceEpochDeaths,
      //      config.stubSyncIntervalMs, config.stubSyncBatchThreshold,
      //      config.continuationProbability,
      //      config.fightTimeoutMs, config.localStatsIntervalMs, config.masterStatsIntervalMs,
      //      config.topNStrategies
      //, config.availableHosts.size,
      //      config.initialStrategy(), stats.bestStrategies
    ).map(_.toString.replaceAll("\\t", "\\t").replaceAll("\\n", "\\n").replaceAll("\"", "\\\"").replaceAll("\\.", "\\,")).map(s => s""""$s"""").mkString(",")
    for ((strategyName, strategyEnergy) <- energyPerStrategy) {
      line = line.concat(",\"").concat(strategyEnergy.prisonersCount.toString.replaceAll("\\.", "\\,")).concat("\"")
    }
    for ((strategyName, strategyEnergy) <- energyPerStrategy) {
      line = line.concat(",\"").concat(strategyEnergy.averageCoop.toString.replaceAll("\\.", "\\,")).concat("\"")
    }
    for ((strategyName, strategyEnergy) <- energyPerStrategy) {
      line = line.concat(",\"").concat(strategyEnergy.averageEnergy.toString.replaceAll("\\.", "\\,")).concat("\"")
    }

    new FileWriter(filename, true) {
      write(line); write("\n"); close()
    }
  }

  def startComputation(actorSystem: ActorSystem, config: IpdConfig, warmUp: FiniteDuration, end: FiniteDuration, finalStats: FiniteDuration,
                       csvStatsInterval: FiniteDuration, csvFilename: String): Unit = {
    val uuid: UUID = UUID.randomUUID()
    val start = System.nanoTime()
    val arena = actorSystem.actorOf(MasterArenaActor.props(config), "arena")
    val masterStats: ActorSelection = actorSystem.actorSelection(s"/user/arena/${MasterArenaActor.MasterStatsName}")
    arena ! MasterArenaActor.Init
    val csvCancel = actorSystem.scheduler.schedule(0 hours, csvStatsInterval, new Runnable {
      override def run(): Unit = {
        import akka.pattern.ask
        implicit val timeout: Timeout = Timeout(30 seconds)
        (masterStats ? MasterStatsActor.CurrentStats) onComplete {
          case Success(stats: MasterStats) =>
            val s: Long = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - start)
            writeCsvStats(csvFilename, uuid, sequential = false, config, stats, s, `final` = false, warmUp, end, finalStats)
            logger.info(stats.pretty)
          case _ => //ignore
        }
      }
    })
    actorSystem.scheduler.scheduleOnce(warmUp, new Runnable {
      override def run(): Unit = {
        logger.info("Starting stats")
        arena ! MasterStatsActor.StartStats
      }
    })
    actorSystem.scheduler.scheduleOnce(end, new Runnable {
      override def run(): Unit = {
        logger.info("Stopping system")
        arena ! MasterArenaActor.Stop
      }
    })
    actorSystem.scheduler.scheduleOnce(finalStats, new Runnable {
      override def run(): Unit = {
        import akka.pattern.ask
        implicit val timeout: Timeout = Timeout(30 seconds)
        logger.info("Ask stats")
        csvCancel.cancel()
        masterStats ? MasterStatsActor.CurrentStats onComplete {
          case Success(stats: MasterStats) =>
            val s: Long = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - start)
            writeCsvStats(csvFilename, uuid, sequential = false, config, stats, s, `final` = true, warmUp, end, finalStats)
            logger.info("=============================================")
            logger.info("============ Final stats ====================")
            logger.info("=============================================")
            logger.info(stats.pretty)
            arena ! MasterArenaActor.Kill
          case _ =>
            arena ! MasterArenaActor.Kill
        }
      }
    })
  }

  def main(args: Array[String]): Unit = {
    val configName: String = args(0)
    val externalConfig: Boolean = args(1).toBoolean
    val ipdConfigName: Option[String] = if (args.length > 2) Some(args(2)) else None

    val warmup: Int = Try(args(3).toInt).getOrElse(60)
    val simulation: Int = Try(args(4).toInt).getOrElse(60)
    val csvInterval: Int = Try(args(5).toInt).getOrElse(10)
    val csvFilename: String = Try(args(6)).getOrElse("stats.csv")

    val akkaConfig = if (externalConfig) ConfigFactory.parseFile(new File(configName)) else ConfigFactory.load(configName)
    val actorSystem: ActorSystem = ActorSystem(name = pl.edu.agh.hdpt.actors.actorSystemName, akkaConfig)

    ipdConfigName.foreach { conf =>
      val config: IpdConfig = IpdFileConfigLoader.get(conf)
      config.validate()

      val statsPrint = (warmup + simulation + (config.localStatsIntervalMs + config.masterStatsIntervalMs) / 1e3 + 5).toInt

      startComputation(actorSystem, config, warmup seconds, (warmup + simulation) seconds, statsPrint seconds, csvInterval seconds, csvFilename)
    }
  }
}
