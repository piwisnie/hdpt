package pl.edu.agh.hdpt.model

import pl.edu.agh.hdpt.actors.PrisonerActor.{Action, Cooperate, Defect}

import scala.util.Random

object Strategies {

  object InitStrategy extends Enumeration {
    val AllCooperate, AllDefect, TitForTat, ccCCCDDDDC, Random = Value
  }

  def getStrategy(strategyName: String) = {
    try {
      InitStrategy.withName(strategyName) match {
        case InitStrategy.AllCooperate => allCooperate
        case InitStrategy.AllDefect => allDefect
        case InitStrategy.TitForTat => titForTat
        case InitStrategy.ccCCCDDDDC => ccCCCDDDDC
        case InitStrategy.Random => random
      }
    } catch {
      case e: NoSuchElementException => createStrategy(strategyName)
    }
  }

  def createStrategy(strategyName: String): Strategy = {
    val strategy_parts: Array[String] = strategyName.split('_')

    val initMoves: Array[Double] = new Array(strategy_parts(1).length)
    for (i <- initMoves.indices) {
      initMoves(i) = if (strategy_parts(1)(i).equals('c')) 1 else 0
    }

    var actionProps: Map[String, Double] = Map.empty
    val movesSize = strategy_parts(0)(0).asDigit + strategy_parts(0)(1).asDigit
    val mapSize = math.pow(2, movesSize).toInt
    for (i <- 0 until mapSize) {
      val actionBinary = i.toBinaryString
      val action = ("0" * (movesSize - actionBinary.length) + actionBinary).replace('0', 'C').replace('1', 'D')
      actionProps = if (strategy_parts(2)(i).equals('C')) actionProps + (action -> 1) else actionProps + (action -> 0)
    }

    new Strategy(
      myMovesLength = strategy_parts(0)(0).asDigit, rivalMovesLength = strategy_parts(0)(1).asDigit,
      initMoves = initMoves, strategyName = strategyName, actionProps = actionProps)
  }

  def random: Strategy = {
    new Strategy(Map("C" -> Random.nextDouble(), "D" -> Random.nextDouble()), initMoves = Array(Random.nextDouble()), strategyName = "Random")
  }

  def titForTat: Strategy = {
    new Strategy(Map("C" -> 1, "D" -> 0), initMoves = Array(1), strategyName = "TitForTat")
  }

  def suspiciousTitForTat: Strategy = {
    new Strategy()
  }

  def grim: Strategy = {
    new Strategy()
  }

  def constStrategy(action: Action, actionProps: Map[String, Double], initAction: Double, strategyName: String): Strategy = {
    new Strategy(actionProps = actionProps, Array(initAction), strategyName = strategyName)
  }

  def allCooperate: Strategy =
    constStrategy(Cooperate, Map("C" -> 1, "D" -> 1), 1, "AllCooperate")

  def allDefect: Strategy =
    constStrategy(Defect, Map("C" -> 0, "D" -> 0), 0, "AllDefect")

  def ccCCCDDDDC: Strategy = {
    val initMoves: Array[Double] = Array(1, 1)
    val actionProps: Map[String, Double] = Map("CCC" -> 1, "CCD" -> 1, "CDC" -> 1, "CDD" -> 0,
      "DCC" -> 0, "DCD" -> 0, "DDC" -> 0, "DDD" -> 1)
    new Strategy(
      initMoves = initMoves, actionProps = actionProps, myMovesLength = 1, rivalMovesLength = 2)
  }
}
