package pl.edu.agh.hdpt.model.map

import pl.edu.agh.hdpt.model.Strategy.Cooperativity
import pl.edu.agh.hdpt.actors.TraversableOnceExt

import scala.util.Random

object SimilarCooperativityNetwork {
  trait CooperativityProvider {
    def prisonersCooperativity(): IndexedSeq[Cooperativity]
  }
}

class SimilarCooperativityNetwork extends NetworkMap {
  import pl.edu.agh.hdpt.model.map.SimilarCooperativityNetwork._

  private var prisonersCount: Int = _
  private var structure: Double = _
  private var cooperativityProvider: CooperativityProvider = _

  override def init(prisonersCount: Int, options: Map[String, Any]): Unit = {
    require(options.contains("structure"))
    require(options.contains("cooperativityProvider") && options("cooperativityProvider").isInstanceOf[CooperativityProvider])
    this.prisonersCount = prisonersCount
    structure = options("structure") match {
      case v: Integer => v.toDouble
      case v: Double => v
    }
    cooperativityProvider = options("cooperativityProvider").asInstanceOf[CooperativityProvider]
  }

  override def findNeighbours(prisoner: Int): IndexedSeq[Int] = {
    def epsEq(x: Double, y: Double): Boolean =
      Math.abs(x - y) < 1e-6

    if (Random.nextDouble() <= structure) {
      val cooperativityOfPrisoners = cooperativityProvider.prisonersCooperativity()
      val coop = cooperativityOfPrisoners(prisoner)
      val minDiff = Math.min(cooperativityOfPrisoners.take(prisoner).map(t => math.abs(coop - t)).minOrElse(1), cooperativityOfPrisoners.drop(prisoner+1).map(t => math.abs(coop - t)).minOrElse(1))
      (0 until cooperativityOfPrisoners.size)
        .filter(idx => idx != prisoner && (epsEq(cooperativityOfPrisoners(idx), minDiff + coop) || epsEq(cooperativityOfPrisoners(idx), coop - minDiff)))
    } else {
      var idx = Random.nextInt(prisonersCount)
      while (idx == prisoner) idx = Random.nextInt(prisonersCount)
      Vector(idx)
    }
  }
}
