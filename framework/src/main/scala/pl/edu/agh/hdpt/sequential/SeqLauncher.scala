package pl.edu.agh.hdpt.sequential

import java.util.concurrent.TimeUnit
import java.util.{Date, UUID}

import pl.edu.agh.hdpt.Launcher
import pl.edu.agh.hdpt.actors.MasterStatsActor.{MasterStats, StrategyInfo}
import pl.edu.agh.hdpt.actors.PrisonerActor
import pl.edu.agh.hdpt.config.{IpdConfig, IpdFileConfigLoader}
import pl.edu.agh.hdpt.model.Strategy.Cooperativity
import pl.edu.agh.hdpt.model.map.EnergyProportionalSamplingMap.{PrisonersEnergyProvider, StrategyStats}
import pl.edu.agh.hdpt.model.map.NetworkMap
import pl.edu.agh.hdpt.model.map.SimilarCooperativityNetwork.CooperativityProvider
import pl.edu.agh.hdpt.model.{Mutations, Strategy}

import scala.collection.immutable.Seq
import scala.collection.mutable
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps
import scala.util.{Random, Try}

object SeqLauncher {
  class SeqCooperativityProvider(prisoners: Seq[Prisoner]) extends CooperativityProvider {
    override def prisonersCooperativity(): IndexedSeq[Cooperativity] =
      prisoners.map(p => p.strategyView().cooperativity).toIndexedSeq
  }

  class SeqPrisonersEnergyProvider(prisoners: Seq[Prisoner]) extends PrisonersEnergyProvider {
    override def localStrategies(): IndexedSeq[StrategyStats] = {
      prisoners.map(p => StrategyStats(p.index, p.energy)).toIndexedSeq
    }
  }

  def main(args: Array[String]): Unit = {
    val uuid: UUID = UUID.randomUUID()

    val timeLimit: Int = args(0).toInt
    val config: IpdConfig = IpdFileConfigLoader.get(args(1))
    config.validate()
    val csvInterval: Int = Try(args(2).toInt).getOrElse(1)
    val csvFilename: String = Try(args(3)).getOrElse("stats.csv")
    val fights = mutable.ArrayBuffer.fill(config.populationSize)(0)
    val fightsSinceMutation = mutable.ArrayBuffer.fill(config.populationSize)(0)
    val energyChangeSinceMutation = mutable.ArrayBuffer.fill(config.populationSize)(0.0)
    val mutations = mutable.ArrayBuffer.fill(config.populationSize)(0)
    var deaths: Int = 0
    var allFights: Long = 0
    var allMutations: Long = 0
    var allIpdsFights: Long = 0
    var allHawkDoveFights: Long = 0
    var allOtherFights: Long = 0
    val initialStrategies = config.initialStrategies
    var prisoners: Seq[Prisoner] = Vector.empty
    for ((strategyName,prisonersCount) <- initialStrategies) {
      val nextStrategyPrisoners: Seq[Prisoner] = Vector.tabulate(prisonersCount.asInstanceOf[Int])(
        n => new Prisoner(n + prisoners.size, config.initialEnergy, config.nextStrategy(strategyName))
      )
      prisoners = prisoners++nextStrategyPrisoners
    }
    prisoners = Random.shuffle(prisoners)

    val cooperativityProvider = new SeqCooperativityProvider(prisoners)
    val localStrategiesProvider = new SeqPrisonersEnergyProvider(prisoners)
    val networkMap: NetworkMap = config.map.newInstance().asInstanceOf[NetworkMap]
    networkMap.init(
      config.populationSize,
      config.mapOptions
        .updated("cooperativityProvider", cooperativityProvider)
        .updated("localStrategiesProvider", localStrategiesProvider)
    )
    val crossoverMap: NetworkMap = config.crossoverMap.newInstance().asInstanceOf[NetworkMap]
    crossoverMap.init(
      config.populationSize,
      config.crossoverMapOptions
        .updated("cooperativityProvider", cooperativityProvider)
        .updated("localStrategiesProvider", localStrategiesProvider)
    )

    val start = System.nanoTime()
    def fromStart(): Long = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - start)
    var lastStats = fromStart()

    Launcher.writeInitLine(csvFilename, initialStrategies)
    stats(false)
    while (fromStart() < timeLimit) {
      while (fights.sum.toDouble / mutations.sum > config.targetFightsToMutationsRatio) {
        prisoners.foreach { prisoner =>
          mutate(prisoner)
        }
      }
      prisoners.foreach { prisoner =>
        val neighbours = networkMap.findNeighbours(prisoner.index).map(prisoners)
        neighbours.filter(_.index < prisoner.index).foreach { neighbour =>
          if (config.separateStrategies) {
            prisoner.updateStrategy(prisoner.strategyView().copy(), None)
            neighbour.updateStrategy(neighbour.strategyView().copy(), None)
          }
          do {
            fight(prisoner, neighbour)
          } while (Random.nextDouble() < config.continuationProbability)
        }
        val currentFromStart = fromStart()
        if ((currentFromStart - lastStats) / csvInterval >= 1) {
          stats(false)
          lastStats = fromStart()
        }
      }
      if (config.forceEpochDeaths) {
        val worstPrisoners = prisoners.sortBy(- _.energy).take(prisoners.size / 10)
        worstPrisoners.foreach(die)
      }
    }
    stats(true)


    def mutate(prisoner: Prisoner) = {
      prisoner.updateStrategy(
        Mutations.mutate(prisoner.strategyView()),
        if (config.replaceEnergyOnMutation) Some(config.initialEnergy) else None
      )
      mutations(prisoner.index) += 1
      allMutations += 1
      fightsSinceMutation(prisoner.index) = 0
      energyChangeSinceMutation(prisoner.index) = 0
    }

    def fight(prisoner: Prisoner, neighbour: Prisoner) = {
      val moveA = prisoner.nextAction()
      fights(prisoner.index) += 1
      fightsSinceMutation(prisoner.index) += 1
      val moveB = neighbour.nextAction()
      fights(neighbour.index) += 1
      fightsSinceMutation(neighbour.index) += 1

      allFights += 1

      val (payoffA, payoffB) = PrisonerActor.Action.resolveFight(choosePayoffs(prisoner, neighbour))(moveA, moveB)
      energyChangeSinceMutation(prisoner.index) += payoffA
      energyChangeSinceMutation(neighbour.index) += payoffB
//      prisoner.payoff(payoffA, moveB)
//      neighbour.payoff(payoffB, moveA)
      if (prisoner.payoff(payoffA, moveB)) die(prisoner)
      if (neighbour.payoff(payoffB, moveA)) die(neighbour)
    }

    def choosePayoffs(prisoner: Prisoner, neighbour: Prisoner) = {
      val ipdProbs = prisoner.strategy.payoffProbs(0) + neighbour.strategy.payoffProbs(0)
      val hawkDoveProbs = prisoner.strategy.payoffProbs(0) + neighbour.strategy.payoffProbs(0) + prisoner.strategy.payoffProbs(1) + neighbour.strategy.payoffProbs(1)

      val payoffProbs = Random.nextDouble()
      if (ipdProbs / 2 > payoffProbs) {
        allIpdsFights += 1
        config.ipdPayoffs
      } else if (hawkDoveProbs / 2 > payoffProbs){
        allHawkDoveFights += 1
        config.hawkDovePayoffs
      } else {
        allOtherFights += 1
        config.otherPayoffs
      }
    }

    def die(prisoner: Prisoner): Unit = {
      resetPrisoner(prisoner)
      deaths += 1
      fights(prisoner.index) = 0
      fightsSinceMutation(prisoner.index) = 0
      mutations(prisoner.index) = 0
      energyChangeSinceMutation(prisoner.index) = 0
    }

    def resetPrisoner(prisoner: Prisoner): Unit = {
//      val neighbors = crossoverMap.findNeighbours(prisoner.index)
//      val strategy: Strategy = neighbors.length match {
//        case n if n >= 2 =>
//          val i = Random.nextInt(n)
//          val j = {
//            var tmp = i
//            while (i == tmp) tmp = Random.nextInt(n)
//            tmp
//          }
//
//          Strategy.crossover(prisoners(i).strategyView(), prisoners(j).strategyView(), prisoner.strategy.strategyName)
//        case 1 =>
//          prisoners(neighbors.head).strategyView().copy(strategyName = prisoner.strategy.strategyName)
//        case _ =>
//          prisoner.strategyView().copy(strategyName = prisoner.strategy.strategyName)
//      }
      val bestStrategy = getBestStrategy(prisoner)


      prisoner.reset(Mutations.mutate(bestStrategy), config.initialEnergy)
    }

    def getBestStrategy(prisoner: Prisoner) = {
      if (config.leavePreviousStrategy){
        val strategyPrisoners = prisoners.filter(_.strategy.strategyName.equals(prisoner.strategy.strategyName))
        val goodStrategy = strategyPrisoners.sortBy(_.energy).map(_.strategy).take(strategyPrisoners.size / 2)(Random.nextInt(strategyPrisoners.size / 2))
        Strategy.crossover(prisoner.strategyView(), goodStrategy, config.leavePreviousStrategy)
      } else {
        val goodStrategy = prisoners.sortBy(_.energy).map(_.strategy).take(prisoners.size / 2)(Random.nextInt(prisoners.size / 2))
        Strategy.crossover(prisoner.strategyView(), goodStrategy, config.leavePreviousStrategy)
      }
    }


    def stats(f: Boolean): Unit = {
      val energies = prisoners.map(_.energy)
      val energySum = energies.sum
      val lastMovesCoops = prisoners.flatMap(_.lastMoves).count(_ == PrisonerActor.Cooperate)
      val lastMoves = prisoners.flatMap(_.lastMoves).count(_ != null)
      var energyPerStrategy: Map[String, StrategyStatistics] = Map.empty
      for ((strategyName,_) <- initialStrategies)
      {
        val strategyStatistics = new StrategyStatistics
        strategyStatistics.averageEnergy = calculateEnergy(prisoners, strategyName)
        strategyStatistics.averageCoop = calculateCoop(prisoners, strategyName)
        strategyStatistics.prisonersCount = calculatePrisonersCount(prisoners, strategyName)
        energyPerStrategy = energyPerStrategy + (strategyName -> strategyStatistics)
      }

      val lastMovesFixed = if (lastMoves > 0) lastMoves else 1
      val stats: MasterStats = MasterStats(new Date((start / 1e3).toLong), config.populationSize,
        energySum, energySum.toDouble / config.populationSize, energies.max, energies.min,
        allFights, fights.sum / 2, fights.sum / config.populationSize, fights.max, fights.min,
        0, 0, 0, lastMovesCoops, lastMovesFixed, deaths,
        allMutations, mutations.sum, mutations.sum.toDouble / config.populationSize, mutations.max, mutations.min,
        prisoners.map(_.strategyView().cooperativity).sum / config.populationSize, (allIpdsFights.toDouble / allFights * 1000).round / 1000.toDouble,
        (allHawkDoveFights.toDouble / allFights * 1000).round / 1000.toDouble, (allOtherFights.toDouble / allFights * 1000).round / 1000.toDouble,
        prisoners.sortBy(_.energy).reverse
          .map(p => StrategyInfo(p.index.toString, p.energy, energyChangeSinceMutation(p.index), fightsSinceMutation(p.index), p.strategyView()))
          .take(config.topNStrategies)
      )
      println(stats.pretty)
      Launcher.writeCsvStats(csvFilename, uuid, sequential = true, config, stats, lastStats, `final` = f, 0 seconds, timeLimit seconds, timeLimit seconds, energyPerStrategy)
    }

    def calculateEnergy(prisoners: Seq[Prisoner], strategyName: String): Double = {
      val allPrisonersEnergy: Double = prisoners
      .filter(_.strategy.strategyName.equals(strategyName))
      .map(_.energy)
      .sum
      val prisonersCount: Double = initialStrategies(strategyName).toString.toDouble
      (allPrisonersEnergy / prisonersCount * 1000).round / 1000.toDouble
    }

    def calculateCoop(prisoners: Seq[Prisoner], strategyName: String): Double = {
      val lastMovesCoops: Double = prisoners.filter(_.strategy.strategyName.equals(strategyName)).flatMap(_.lastMoves).count(_ == PrisonerActor.Cooperate)
      val lastMoves: Double = prisoners.filter(_.strategy.strategyName.equals(strategyName)).flatMap(_.lastMoves).count(_ != null)

      (lastMovesCoops / lastMoves * 1000).round / 1000.toDouble
    }

    def calculatePrisonersCount(prisoners: Seq[Prisoner], strategyName: String): Int = {
      prisoners.filter(_.strategy.strategyName.equals(strategyName)).count(_ != null)
    }
  }
}
