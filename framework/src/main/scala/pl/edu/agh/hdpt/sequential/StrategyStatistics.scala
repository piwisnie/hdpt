package pl.edu.agh.hdpt.sequential

class StrategyStatistics {
  var averageEnergy: Double = 0
  var averageCoop: Double = 0
  var prisonersCount: Int = 0
  var allFights: Int = 0
  var ipdFights: Int = 0
}
