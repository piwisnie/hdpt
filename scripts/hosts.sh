#!/bin/bash

cd $HOME/ipd

INSTANCES_PER_NODE=$1
WARMUP=$2
RUN=$3
CSV_INTERVAL=$4
ARGS=($@)
CONFIGS=("${ARGS[@]:4}")

NODES_LIST=`scontrol show hostname $SLURM_NODELIST | paste -d,`
NODES_COUNT=`scontrol show hostname $SLURM_NODELIST | wc -l`

HOSTS="availableHosts = ["
for HOST in $NODES_LIST; do
  for ID in $(seq 1 $INSTANCES_PER_NODE); do
    PORT=`expr $ID + 2551`
    HOSTS=${HOSTS}'"'${HOST}":"${PORT}'",'
  done;
done;
HOSTS=${HOSTS}"]"
EMPTY="availableHosts = \[\]"

for IPD_CONFIG_TEMPLATE_NAME in "${CONFIGS[@]}"; do
  IPD_CONFIG_TARGET_NAME="gen_${NODES_COUNT}_${IPD_CONFIG_TEMPLATE_NAME}"
  CSV_NAME="$IPD_CONFIG_TARGET_NAME.csv"
  sed -e "s/$EMPTY/$HOSTS/g" $IPD_CONFIG_TEMPLATE_NAME > $IPD_CONFIG_TARGET_NAME
  srun node.sh $INSTANCES_PER_NODE $IPD_CONFIG_TARGET_NAME $WARMUP $RUN $CSV_INTERVAL $CSV_NAME
done