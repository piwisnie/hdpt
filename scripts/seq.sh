#!/bin/bash

cd $HOME/ipd

RUN=$1
CSV_INTERVAL=$2
ARGS=($@)
CONFIGS=("${ARGS[@]:2}")

NODE_LOG=${SLURM_JOBID}_seq.log

module load plgrid/tools/java8

for IPD_CONFIG_TARGET_NAME in "${CONFIGS[@]}"; do
    CSV_NAME="$IPD_CONFIG_TARGET_NAME.csv"
    java -Xmx4G -cp ".:ipd.jar" pl.edu.agh.ipd.sequential.SeqLauncher $RUN $IPD_CONFIG_TARGET_NAME $CSV_INTERVAL $CSV_NAME > $NODE_LOG
done