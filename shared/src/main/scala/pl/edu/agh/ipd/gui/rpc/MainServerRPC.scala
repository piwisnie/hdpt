package pl.edu.agh.ipd.gui.rpc

import com.avsystem.commons.rpc.RPC
import pl.edu.agh.ipd.gui.config.SimulationConfig
import pl.edu.agh.ipd.gui.stats.Stats

import scala.concurrent.Future

@RPC
trait MainServerRPC {
  def start(config: SimulationConfig): Future[Unit]
  def stop(): Future[Unit]
  def stats(): Future[String] //TODO Future[Stats]
  def resetStats(): Future[Unit]
  def isRunning(): Future[Boolean]
}
