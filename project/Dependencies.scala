import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._
import sbt._

object Dependencies extends Build {
  val udashVersion = "0.4.0"
  val udashJQueryVersion = "1.0.0"
  val scalaCssVersion = "0.4.1"

  val silencerVersion = "0.4"
  val akkaVersion = "2.4.3"
  val scalaLoggerVersion = "3.4.0"
  val jettyVersion = "9.3.8.v20160314"
  val typesafeConfig = "1.3.1"
  val logbackVersion = "1.1.3"
  val reactiveMongoVersion = "0.12-RC0"
  val groovyVersion = "2.4.7"
  val csvParserVersion = "0.1.13"
  val jsoupVersion = "1.9.2"
  val quartzVersion = "2.2.3"
  val javaMailVersion = "1.5.6"
  val scalatagsVersion = "0.6.0"
  val guavaVersion = "21.0"

  val scalatestVersion = "3.0.0"

  val compilerPlugins = Def.setting(Seq(
    "com.github.ghik" % "silencer-plugin" % silencerVersion
  ).map(compilerPlugin))


  val crossDeps = Def.setting(Seq[ModuleID](
    "io.udash" %%% "udash-core-shared" % udashVersion,
    "io.udash" %%% "udash-rpc-shared" % udashVersion
  ))

  val ipdDeps = Def.setting(Seq[ModuleID](
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-remote" % akkaVersion,
    "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,

    "com.github.romix.akka" %% "akka-kryo-serialization" % "0.4.1",

    "org.jgrapht" % "jgrapht-core" % "1.0.0",

    "org.codehaus.groovy" % "groovy-all" % "2.4.6",
    "ch.qos.logback" % "logback-classic" % logbackVersion,
    "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggerVersion,

    "com.github.ghik" % "silencer-lib" % silencerVersion,
    "com.google.guava" % "guava" % guavaVersion,

    // Tests
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
    "org.scalatest" %% "scalatest" % scalatestVersion % Test
  ))

  val frontendDeps = Def.setting(Seq[ModuleID](
    "io.udash" %%% "udash-bootstrap" % udashVersion,
    "io.udash" %%% "udash-core-frontend" % udashVersion,
    "io.udash" %%% "udash-jquery" % udashJQueryVersion,
    "io.udash" %%% "udash-rpc-frontend" % udashVersion,

    "com.github.japgolly.scalacss" %%% "core" % scalaCssVersion,
    "com.github.japgolly.scalacss" %%% "ext-scalatags" % scalaCssVersion
  ))

  val backendDeps = Def.setting(Seq[ModuleID](
    "io.udash" %% "udash-rpc-backend" % udashVersion,

    "org.eclipse.jetty" % "jetty-server" % jettyVersion,
    "org.eclipse.jetty" % "jetty-servlet" % jettyVersion,
    "org.eclipse.jetty.websocket" % "websocket-server" % jettyVersion,

    "ch.qos.logback" % "logback-classic" % logbackVersion,
    "org.codehaus.groovy" % "groovy-all" % groovyVersion
  ))
}