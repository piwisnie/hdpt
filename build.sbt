name := "hdpt"

version in ThisBuild := "0.1.0-SNAPSHOT"
scalaVersion in ThisBuild := "2.11.7"
organization in ThisBuild := "pl.edu.agh"
scalacOptions in ThisBuild ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  "-language:implicitConversions",
  "-language:existentials",
  "-language:dynamics",
  "-Xfuture",
  "-Xfatal-warnings",
  "-Xlint:_,-missing-interpolator,-adapted-args"
)

def crossLibs(configuration: Configuration) =
  libraryDependencies ++= crossDeps.value.map(_ % configuration)

val ipd = project.in(file("."))
  .aggregate(framework)
  .settings(
    publishArtifact := false,
    libraryDependencies ++= compilerPlugins.value,
    libraryDependencies ++= ipdDeps.value
  )

lazy val framework = project.in(file("framework"))
  .settings(
    libraryDependencies ++= compilerPlugins.value,
    libraryDependencies ++= ipdDeps.value,

    mainClass := Some("pl.edu.agh.ipd.Launcher"),
    assemblyJarName := "ipd.jar",
    assemblyMergeStrategy in assembly := {
      case x if x.endsWith(".xml") => MergeStrategy.concat
      case x =>
        val oldStrategy = (assemblyMergeStrategy in assembly).value
        oldStrategy(x)
    }
  )

lazy val shared = crossProject.crossType(CrossType.Pure).in(file("shared"))
  .settings(
    crossLibs(Provided)
  )


